# Custom F-Droid Repository

This uses the GitLab CI/CD tool to automatically download APKs and publishes them in the form of an F-Droid repository on Gitlab Pages.

Is is based on Ricki Hirner's [fdroid-firefox](https://gitlab.com/rfc2822/fdroid-firefox) project. But instead of only fetching APKs from official places, they can also be downloaded from the [apkpure.com](https://apkpure.com) website. This let's you easily add almost any app to the repository, even if there is no official download page.

To verify the downloaded APKs, the SHA256 fingerprint of the signing certificate is checked. So one has to get an APK from a legitimate source once, in order to obtain the fingerprint. E.g. from a second phone that has Play Store installed or from something like Aurora Store. Check out the shell script in this repo on how to do this.

## How to use it
* Fork this repo.
* Create a keystore. This keystore contains the certificate and the private key that is used to sign the F-Droid repository. (you may need to install Java for this): `keytool -genkey -v -keystore keystore.p12 -alias repokey -keyalg RSA -keysize 4096 -sigalg SHA256withRSA -validity 10000 -storetype pkcs12`

* Go to **Settings > CI/CD**, expand the **Variables** section and add the following variables:

| Key                    | Value                                |
| ---------------------- | ------------------------------------ |
| FDROID_KEYSTOREPASS    | Password used to create the keystore |
| FDROID_KEYSTORE_BASE64 | Output of `base64 keystore.p12`      |

* Go to **CI/CD > Schedules** and create a daily pipeline.
* Edit `apps.yml` and add the apps you want in your repository.
* After the pipeline had run, the repository should be available at https://your_gitlab_username.gitlab.io/custom-repo/fdroid/repo.
* To get the the certificate fingerprint to add the repository to F-Droid run `keytool -list -keystore keystore.p12 -alias repokey | grep SHA | cut -d' ' -f4 | tr -d ':'`
