from docker.io/debian:buster-slim
RUN mkdir -p /usr/share/man/man1
RUN apt-get -qq update && apt-get -qq --no-install-recommends install \
   git \
   rsync \
   curl \
   default-jdk-headless \
   python3-pip \
   python3-setuptools \
   python3-selenium \
   firefox-esr
RUN python3 -m pip -qq install pip==21.1
RUN python3 -m pip -qq install fdroidserver==2.0.1
RUN curl -sSL "https://github.com/mozilla/geckodriver/releases/download/v0.29.1/geckodriver-v0.29.1-linux64.tar.gz" | tar -xz -C /usr/local/bin
